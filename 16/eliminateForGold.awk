match($0, "children: ([[:digit:]]+)", a) { if(a[1] != 3) { next; } }
match($0, "cats: ([[:digit:]]+)", a) { if(a[1] <= 7) { next; } }
match($0, "samoyeds: ([[:digit:]]+)", a) { if(a[1] != 2) { next; } }
match($0, "pomeranians: ([[:digit:]]+)", a) { if(a[1] >= 3) { next; } }
match($0, "akitas: ([[:digit:]]+)", a) { if(a[1] != 0) { next; } }
match($0, "vizslas: ([[:digit:]]+)", a) { if(a[1] != 0) { next; } }
match($0, "goldfish: ([[:digit:]]+)", a) { if(a[1] >= 5) { next; } }
match($0, "trees: ([[:digit:]]+)", a) { if(a[1] <= 3) { next; } }
match($0, "cars: ([[:digit:]]+)", a) { if(a[1] != 2) { next; } }
match($0, "perfumes: ([[:digit:]]+)", a) { if(a[1] != 1) { next; } }
//{ print $1, $2; }
