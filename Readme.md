Working through [adventofcode.com](http://adventofcode.com)
==================================

Day 1 - [Gnu Smalltalk](http://smalltalk.gnu.org/)
---------------------

    $ gst elevator.st

Day 2 - [awk](https://www.gnu.org/software/gawk/manual/gawk.html)
--------------------

    $ ./checkwrap < input

Day 3 - [Rust](https://www.rust-lang.org)
--------------

    $ rustc sleighride.rs
    $ ./sleighride input

Day 4 - [Bash](https://www.gnu.org/software/bash/) & [Ruby](https://www.ruby-lang.org) & [Python](https://www.python.org)
-------------

    $ ./miner.sh
    ...and wait... and wait... and wait...

or just

    $ ruby rubyminer.rb 00000
    $ ruby rubyminer.rb 000000

and even faster

    $ python pythonminer.py 00000
    $ python pythonminer.py 000000

Had to rewrite from Bash into Ruby in order to get the gold star. I was averaging about 700 hashes/sec with the bash script. The ruby code averages over 850,000 hashes/sec, or over 1200x faster; the python miner, faster still: over 1 million hashes per sec.

Day 5 - [Perl](https://www.perl.org/)
--------------

    $ perl nicenames.pl < input

Day 6 - [Ruby](https://www.ruby-lang.org) (For real this time)
--------------------------------------------------------------

    $ ./lights.rb input

Day 7 - [GNU Prolog](http://www.gprolog.org/)
--------------------

    $ sed -n -f convert.sed < input > convert.pro
    $ prolog
    ?- [circuit,convert].
    ?- probe(lx,V).

Day 8 - [Lisp - SBCL](http://www.sbcl.org)
---------------------
This requires [quicklisp](https://www.quicklisp.org).

    $ sbcl --load stringcounts.lisp
    * (exit)

Day 9 - [Haskell](https://www.haskell.org/)
-----------------
Um, this day is still a work in progress...

Day 10 - [Java](http://www.oracle.com/technetwork/java/javase/overview/index.html)
---------------

    $ ./gradlew run

Day 11 - [TypeScript](http://www.typescriptlang.org/)
---------------------
Requires [Node.js](https://nodejs.org) for runtime.

    $ npm install
    $ ./run

Day 12 - [Lua](http://www.lua.org/)
--------------

    $ lua NoJson.lua

Day 13 - [OCaml](http://ocaml.org/)
----------------
This day is also still a work in progress. I have problems with NP complete problems. :(

Day 14 - [C#](https://msdn.microsoft.com/en-us/library/kx37x362.aspx)/[Mono](http://www.mono-project.com/)
-------------
For non MS platforms:

    $ mcs reindeerRace.cs
    $ mono reindeerRace.exe input

Day 16 - [Awk](https://www.gnu.org/software/gawk/manual/gawk.html) again, sorry.
------------------------------
These problems deal a lot with pattern matching, so awk is a natural fit.

    $ awk -f eliminate.awk < input
    $ awk -f eliminateForGold.awk < input

### Potential Languages
* ANSI C
* Erlang
* <del>Haskell</del>
* <del>Common Lisp</del>
* <del>Smalltalk</del>
* Scheme
* D
* C++
* <del>Java</del>
* Scala
* Clojure
* <del>C#</del>
* GoLang
* <del>Rust</del>
* Dart
* <del>Ruby</del>
* Python
* PHP
* Javascript/node
* <del>Typescript/Coffee Script</del>
* Objective-C
* Swift
* Forth
* <del>Perl</del>
* Perl 6
* <del>Bash</del>
* SQL
* <del>Lua</del>
* Haxe
* <del>sed/awk</del>
* <del>OCaml</del>
* <del>Prolog</del>
* Tcl/Tk

### Oh Boy...
* Assembler
* R
* Fortran
* Pascal
* Cobol
* Vim Script
