# Oh boy was going the Bash route wrong...
# Recoded in Ruby, got a 1000x speedup!
# 
require 'digest/md5'

pattern = "00000"

if ARGV.length > 0 then
  pattern = ARGV[0]
end

md5 = ""
idx = 0
key = "yzbqklnj"

start_time = Time.now
while !md5.start_with?(pattern) do
  md5 = Digest::MD5.hexdigest(key + idx.to_s)
  idx += 1
end
diff_time = Time.now - start_time

puts "Found #{md5} at #{idx-1} in #{diff_time.to_i} secs."

if diff_time > 0 then
  puts "Processed #{ (idx/diff_time).to_i } Hashes/sec"
end
