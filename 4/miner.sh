#!/bin/bash
#
# So, BASH here is probably the WORST choice, since the problem
# requires an exhaustive search of the keyspace. So fast MD5
# hashing is paramount. But instead, I'm using bash and the
# external 'md5' program which has to kill performance.
#
########################

if [ "" != "`which md5`" ]; then
  MD5="md5"
elif [ "" != "`which md5sum`" ]; then
  MD5="md5sum"
else
  echo "Unable to locate application to calc md5!"
  exit 500
fi

user_interrupt() {
    echo
    echo "Stopped on $IDX"
    hashps
    exit 1;
}

trap user_interrupt SIGINT

hashps() {
  END_TIME=`date +%s`
  ELAPSED=$(( ($END_TIME - $START_TIME) ))
  HASH_COUNT=$(( $IDX - $START_IDX ))
  if [ $ELAPSED -gt 0 ]; then
    echo "Calculated $(( $HASH_COUNT / $ELAPSED )) Hashes/sec"
  fi
}

KEY="yzbqklnj"
# PATTERN="^00000"
PATTERN="^000000"
echo "Set KEY to $KEY"

if [ $# -gt 0 ]; then
    IDX=$1
    echo "Starting at $IDX"
else
    IDX=0
fi

START_IDX=$IDX
START_TIME=`date +%s`

while [[ ! "$HASH" =~ $PATTERN ]]; do
    case $MD5 in
      "md5")
        HASH=`md5 -q -s "$KEY$IDX"`
        ;;
      "md5sum")
        HASH=`echo -n "$KEY$IDX" | md5sum - | cut -d' ' -f1`
        ;;
    esac
    IDX=$(($IDX+1))
    # echo $HASH
done

echo "$HASH at $(($IDX-1))"
hashps
