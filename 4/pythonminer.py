import hashlib;
import time;
import sys;

pattern = "00000"

if len(sys.argv) > 1:
    pattern = sys.argv[1]

md5 = ""
idx = 0
key = "yzbqklnj"

start_time = time.time()
while not (md5.startswith(pattern)):
    md5 = hashlib.md5(key + str(idx)).hexdigest();
    idx += 1

diff_time = time.time() - start_time

print "Found", md5, "at", idx-1, "in", diff_time, "secs."

if diff_time > 0:
    print "Processed", int(idx/diff_time), "Hashes/sec"
