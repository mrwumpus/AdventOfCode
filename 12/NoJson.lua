io.input("input")

function sumNums (line)
    local sum = 0
    local var = ""
    local s, e = string.find(line, "-*%d+")
    while s do
        var = string.sub(line, s, e) 
        sum = sum + var
        -- print (var)
        s, e = string.find(line, "-*%d+", e+1)
    end

    return sum
end

-- Oh, boy... check out this mess!
function sumObject (line, start, nored)
    -- print(string.format("Found object at %d", start))
    local chars = ""
    local key = false
    local value = false
    local red = false
    local inString = false
    local sum = 0;
    local skipIdx = 0;
    for i = start, #line do
        local c = line:sub(i,i)
        if i < skipIdx then
            -- print("skip index")
            -- nop, skip
        elseif '"' == c and value and inString then
            -- print(string.format("Found string value: %s", chars))
            red = red or (chars:len() > 0 and "red" == chars)
            chars = ""
            value = false
            key = false
            inString = false
        elseif '"' == c and key and inString then
            -- print("quote and key and in string")
            chars = ""
            inString = false
        elseif '"' == c then
            --print("Quote")
            key = true
            inString = not inString
        elseif not value and inString or ' ' == c then
            -- print(string.format("Skipping %s at %d because it's inString", c, i))
            -- nop
        elseif ':' == c then
            -- print("Colon")
            value = true
        elseif ',' == c then
            -- print("comma")
            if chars:len() > 0 then
                sum = sum + chars
            end
            value = false
        elseif '[' == c then
            local arraySum, arrIdx = sumArray(line, i+1, nored)
            sum = sum + arraySum
            skipIdx = arrIdx
        elseif '{' == c then
            local objSum, objIdx = sumObject(line, i+1, nored)
            sum = sum + objSum
            skipIdx = objIdx
        elseif '}' == c then
            if nored and red then
                return 0, i+1
            elseif chars:len() > 0 then
                -- print(string.format("object %d, %d", sum+chars, i+1))
                return sum + chars, i+1
            else
                -- print(string.format("object %d, %d", sum, i+1))
                return sum, i+1
            end
            --end
        else
            -- print("building chars...")
            chars = string.format("%s%s", chars, c)
        end
    end
end

-- and this mess, too!
function sumArray (line, start, nored)
    -- print(string.format("Found array at %d", start))
    local chars = ""
    local sum = 0
    local inString = false
    local skipIdx = 0
    for i = start, #line do
        local c = line:sub(i,i)
        if i < skipIdx then
            -- nop, process through object
        elseif '"' == c then
            inString = not inString
        elseif inString or ' ' == c then
            -- nop, ignore
        elseif ']' == c then
            if chars:len() > 0 then
                return sum + chars, i+1
            else
                return sum, i+1
            end
        elseif ',' == c then
            -- print(string.format("Chars be %s", chars))
            if chars:len() > 0 then
                sum = sum + chars
                chars = ""
            end
        elseif '{' == c then
            local objSum, endIdx = sumObject(line, i+1, nored)
            sum = sum + objSum
            skipIdx = endIdx
        elseif '[' == c then
            local objSum, endIdx = sumArray(line, i+1, nored)
            sum = sum + objSum
            skipIdx = endIdx
        else
            chars = string.format("%s%s", chars, c)
        end
    end
end

local sum = 0
local redSum = 0;
for line in io.lines() do
    -- sum = sum + sumNums(line)
    sum = sum + sumArray(line, 2, false);
    redSum = redSum + sumArray(line, 2, true);
end


print(string.format("Sum: %d", sum))
print(string.format("Red Sum: %d", redSum))
-- holy crap, it worked...

-- print(sumArray("[1,2,\"value\",3]", 2));
