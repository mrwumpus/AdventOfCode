/// <reference path="./node-definitions/node.d.ts" />

class Alphabet {
  private static alphabet:string = "abcdefghijklmnopqrstuvwxyz";
  static first():string {
    return Alphabet.alphabet[0];
  }

  static next(letter:string, wrap:boolean = true):string {
    var nextIdx:number = 
      Alphabet.alphabet.indexOf(letter) + 1;
    return nextIdx == Alphabet.alphabet.length
      ? wrap ? Alphabet.alphabet[0] : null
      : Alphabet.alphabet[nextIdx];
  }
}

function roll(pass:string):string {
  var len:number = pass.length,
      nextPass:string = "",
      nextChar:string = "",
      rollIt:boolean = true;

  for(var i = len-1; i >= 0; --i) {
    if(rollIt) {
      nextChar = Alphabet.next(pass[i]);
      if(nextChar != Alphabet.first())
        rollIt = false;
    } else {
      nextChar = pass[i];
    }

    nextPass = nextChar + nextPass;
  }

  return rollIt 
    ? Alphabet.first() + nextPass 
    : nextPass;
}

class PasswordValidator {
  validate(pass:string):boolean {
    return true;
  }
}

class SmallStraightValidator extends PasswordValidator {
  validate(pass:string):boolean {
    var count:number = 0,
        cur:string = "", next:string = "";

    for(var i = 0; i < pass.length; ++i) {
      cur = pass[i];
      if(next && cur == next) {
        if(++count == 2) {
          return true;
        }
      } else {
        count = 0;
      }
      // Don't wrap (from z to a) here.
      next = Alphabet.next(cur, false);
    }
    return false;
  }
}

class NoIOhElValidator extends PasswordValidator {
  validate(pass:string):boolean {
    return pass.indexOf('i') < 0
      && pass.indexOf('o') < 0
      && pass.indexOf('l') < 0;
  }
}

class DoubleDoubleValidator extends PasswordValidator {
  validate(pass:string):boolean {
    var cur:string = "",
        prev:string = "",
        doubles:number = 0;
    for(var i = 0; i < pass.length; ++i) {
      cur = pass[i];
      if(cur == prev) {
        if(++doubles == 2) {
          return true;
        }
        cur = "X"; // This won't match a thing.
      }

      prev = cur;
    }
  }
}

var validators = [new DoubleDoubleValidator(),
                  new NoIOhElValidator(),
                  new SmallStraightValidator()];

function validate(pass:string): boolean {
  for(var i = 0; i < validators.length; ++i) {
    if(!validators[i].validate(pass)) {
      return false;
    }
  }

  return true;
}

function getNext(pass:string): string {
  var goodPass:string = pass;
  while(!validate(goodPass)) {
    goodPass = roll(goodPass);
  }

  return goodPass;
}

var password:string = "testzzzzzzzz";
if(process.argv.length > 2) {
  password = process.argv[2];
}

password = getNext(password);

console.log("Silver Pass: ", password);
console.log("Gold Pass: ", getNext(roll(password)));
