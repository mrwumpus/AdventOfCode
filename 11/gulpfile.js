var gulp = require('gulp');
var del = require('del');
var ts = require('gulp-typescript');

gulp.task('default', function() {
  var result = gulp.src('*.ts')
  .pipe(ts({
    out: 'output.js'
  }))
  .pipe(gulp.dest('build'));
});

gulp.task('clean', function() {
  del.sync(['build']);
});
