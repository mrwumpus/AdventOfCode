#!/usr/bin/ruby
if ARGV.length > 0 then
    $filename = ARGV[0]
else
    raise "Please provide a file name"
end

$length, $width = 1000, 1000

def inc_range(lightArray, x, y, endx, endy, value)
    if x > endx || y > endy
        raise "Uh oh..."
    end

    for dx in x..endx
        for dy in y..endy
            val = lightArray[dx][dy] + value;
            # value can be negative.
            lightArray[dx][dy] = val < 0 ? 0 : val
        end
    end
end

def set_range(lightArray, x, y, endx, endy, value)
    if x > endx || y > endy
        raise "Uh oh..."
    end

    for dx in x..endx
        for dy in y..endy
            lightArray[dx][dy] = value
            #puts "Setting (#{dx},#{dy}) to #{value}"
        end
    end
end
    
def toggle_range(lightArray, x, y, endx, endy)
    if x > endx || y > endy
        raise "Uh oh..."
    end

    for dx in x..endx
        for dy in y..endy
            val = lightArray[dx][dy]
            lightArray[dx][dy] = val == 1 ? 0 : 1
            #puts "Toggling (#{dx},#{dy}) to #{!val}"
        end
    end
end 

def process( for_gold )

    lightArray = Array.new($length) { Array.new($width, 0) }

    File.foreach($filename) do | line |
        line.gsub(/^turn on (\d+),(\d+) through (\d+),(\d+)/) {
            |s| 
            set_range(lightArray, $1.to_i, $2.to_i, $3.to_i, $4.to_i, 1) unless for_gold
            inc_range(lightArray, $1.to_i, $2.to_i, $3.to_i, $4.to_i, 1) if for_gold
        }
        line.gsub(/^turn off (\d+),(\d+) through (\d+),(\d+)/) {
            |s| 
            set_range(lightArray, $1.to_i, $2.to_i, $3.to_i, $4.to_i, 0) unless for_gold
            inc_range(lightArray, $1.to_i, $2.to_i, $3.to_i, $4.to_i, -1) if for_gold
        }
        line.gsub(/^toggle (\d+),(\d+) through (\d+),(\d+)/) {
            |s| 
            toggle_range(lightArray, $1.to_i, $2.to_i, $3.to_i, $4.to_i) unless for_gold
            inc_range(lightArray, $1.to_i, $2.to_i, $3.to_i, $4.to_i, 2) if for_gold
        }
        #puts "#{line}"
    end

    #set_range(499,499,500,50, true);
    #toggle_range(0,0,9,0);
    #toggle_range(0,0,0,9);

    count = lightArray.inject { |sum, arr| Array.new(1) { sum[0] + arr.reduce(:+) } }[0];

    puts "Lights on: #{count}"
end

process( false )
process( true )
