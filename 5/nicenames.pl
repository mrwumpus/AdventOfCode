#!/usr/bin/perl -w

$count = 0;
$improvedCount = 0;

foreach $line ( <stdin> ) {
    chomp( $line );
    # print "$line\n";
    if ( initialNameCheck( $line ) ) {
        $count += 1;
    }

    if ( improvedNameCheck( $line ) ) {
        $improvedCount += 1;
    }
}

print "Found $count nice names!\n";
print "Found $improvedCount improved nice names!\n";

sub initialNameCheck {
    $value = $_[0];
    
    return $value =~ /([aeiou].*){3,}/ &&
    $value =~ /([a-zA-Z])\1/ &&
    $value !~ /ab|cd|pq|xy/;
}

sub improvedNameCheck {
    $value = $_[0];
    
    return $value =~ /(\w\w).*\1/ &&
    $value =~ /(\w)\w\1/;

}
