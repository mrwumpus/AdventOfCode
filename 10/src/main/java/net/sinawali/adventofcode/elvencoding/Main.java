package net.sinawali.adventofcode.elvencoding;

import java.util.stream.Stream;

public class Main {
	
	private static final String[] TEST_STRINGS = {
			"1",
			"11",
			"21",
			"1211",
			"111221"
	};
	
	private static final String SILVER_STAR = "1321131112";
	private static final int SILVER_REPEATS = 40;
	private static final int GOLD_REPEATS = 50;

	public static void main(String[] args) {
		new Main().goForIt();
	}

	private Main() {
		// default private constructor
	}
	
	private void goForIt() {
		System.out.println("Silver: " + elvencodeString(SILVER_STAR, SILVER_REPEATS).length());
		System.out.println("Gold: " + elvencodeString(SILVER_STAR, GOLD_REPEATS).length());
	}

	public void runTestStrings() {
		Stream.of(TEST_STRINGS).forEach(
				str -> System.out.println(
						String.format("%s: %s", str, elvencodeString(str)))
				);
	}
	
	private String elvencodeString(final String str, int times) {
		assert times > 0;
		String inputString = str;
		int count = 0;
		do {
			inputString = elvencodeString(inputString);
			// System.out.println(inputString);
			count += 1;
		}while(count < times);
		
		return inputString;
	}

	private String elvencodeString(final String str) {
		final StringBuilder output = new StringBuilder();

		int count = 0;
		Character currentChar = null;
		for (Character ch : str.toCharArray()) {
			if (!ch.equals(currentChar)) {
				if (currentChar != null) {
					output.append(count).append(currentChar);
				}
				currentChar = ch;
				count = 1;
			} else {
				count += 1;
			}
		}
		
		if(currentChar != null) {
			output.append(count).append(currentChar);
		}

		return output.toString();
	}

}
