use std::env;
use std::error::Error;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;
use std::collections::HashSet;

fn main() {
  let args: Vec<String> = env::args().collect();

  if args.len() < 2 {
    panic!("Please specify the input file")
  }

  // read the input file into the string.
  let file_str: String = read_input_file(Path::new(&args[1]));

  // Create the new point
  let p = Point::new();
  // Collect the points in a hash set so that
  // it will naturally eliminate duplicates.
  let mut hash = HashSet::new();

  // Insert the starting point.
  // Just hashing on the to_string since the hashset naturally
  // supports String hashing and the String representation is unique
  // to its point on the x,y plane.
  hash.insert(p.to_string());

  // Iterate over the charactes, starting at initial point 'p'
  // Use fold, as it will preserve the last calculated point
  // as a parameter to the next iteration.
  file_str.chars().fold(p,
    |p, c|-> Point {
      // Use the current character 'c' to move the current point 'p'
      let got_point = match p.move_it(c) {
        Err(_) => panic!("Error moving point!"),
        Ok(point) => point,
      };

      // Insert the new point into the hash...
      hash.insert(got_point.to_string());
      // Return the new point, will be used during the next
      // iteration as the point to move from.
      return got_point;
    });

  println!("Found {} unique stops", hash.len());

  // This is the 'santa point'
  let mut sp = Point::new();
  // This is the 'robo-santa point'
  let mut rsp = Point::new();

  // Just reuse the old hash.
  hash.clear();

  // Again, insert the origin.
  hash.insert(sp.to_string());

  // Create the toggle to keep track of whose turn it is
  // to move. Santa moves first, to initialize to false.
  let mut robo_turn = false;

  for c in file_str.chars() {
      if robo_turn {
        // for robo-santa, move from the 'rsp' point.
        // set rsp to this new point for the following move.
        rsp = match rsp.move_it(c) {
          Err(_) => panic!("Error moving point!"),
          Ok(point) => point,
        };

        robo_turn = false;
        hash.insert(rsp.to_string());
      } else {
        // for santa, move from the 'sp' point.
        // set sp to this new point for the following move.
        sp = match sp.move_it(c) {
          Err(_) => panic!("Error moving point!"),
          Ok(point) => point,
        };

        robo_turn = true;
        hash.insert(sp.to_string());
      }
    };

    println!("With the help of robo-santa, found {} unique stops", hash.len());
}

// Function to read the input file in.
fn read_input_file(path: &Path) -> String {
  let mut file = match File::open(path) {
    Err(why) => panic!("Couldn't open {}: {}", path.display(),
                                               Error::description(&why)),
    Ok(file) => file,
  };

  let mut file_str = String::new();

  match file.read_to_string(&mut file_str) {
    Err(why) => panic!("Couldn't read {}: {}", path.display(),
                                              Error::description(&why)),
    Ok(_) => {}
  };

  return file_str;
}

struct Point {
  x: i32,
  y: i32,
}

enum MoveError {
  InvalidDirectionCharacter
}

type MoveResult = Result<Point, MoveError>;

impl Point {
  fn new() -> Point {
    Point { x:0, y:0 }
  }

  fn north(&self) -> Point {
    Point { x: self.x, y: self.y+1 }
  }

  fn south(&self) -> Point {
    Point { x: self.x, y: self.y-1 }
  }

  fn east(&self) -> Point {
    Point { x:self.x+1, y: self.y }
  }

  fn west(&self) -> Point {
    Point { x:self.x-1, y: self.y }
  }

  fn move_it(&self, ch: char) -> MoveResult {
    return match ch {
      '^' => Ok(self.north()),
      'v' => Ok(self.south()),
      '<' => Ok(self.west()),
      '>' => Ok(self.east()),
      _ => Err(MoveError::InvalidDirectionCharacter)
    };
  }
}

impl ToString for Point {
  fn to_string(&self) -> String {
    return format!("({},{})", self.x, self.y);
  }
}

