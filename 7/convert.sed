1 i\
:-discontiguous(or/3, wire/2, not/2, and/3, lshift/3, rshift/3).
/^[[:alnum:]]\{1,\} -> [[:alnum:]]\{1,\}/s/^\([[:alnum:]]\{1,\}\) -> \([[:alnum:]]\{1,\}\)/wire(\2,\1)./ p
/NOT/s/NOT \([[:alnum:]]\{1,\}\) -> \([[:alnum:]]\{1,\}\)/not(\1,\2)./ p
/AND/s/\([[:alnum:]]\{1,\}\) AND \([[:alnum:]]\{1,\}\) -> \([[:alnum:]]\{1,\}\)/and(\1,\2,\3)./ p
/OR/s/\([[:alnum:]]\{1,\}\) OR \([[:alnum:]]\{1,\}\) -> \([[:alnum:]]\{1,\}\)/or(\1,\2,\3)./ p
/LSHIFT/s/\([[:alnum:]]\{1,\}\) LSHIFT \([[:alnum:]]\{1,\}\) -> \([[:alnum:]]\{1,\}\)/lshift(\1,\2,\3)./ p
/RSHIFT/s/\([[:alnum:]]\{1,\}\) RSHIFT \([[:alnum:]]\{1,\}\) -> \([[:alnum:]]\{1,\}\)/rshift(\1,\2,\3)./ p
