:-dynamic(probe/2).

probe(1,1).

probe(X,Y) :-
  wire(X,Y),
  write(X), write(' = '), write(Y), nl.

probe(X,Y) :-
  wire(X,Z),
  probe(Z,Y).

probe(X,Y) :-
  and(A,B,X), !,
  write(X), write(' = '), write(A), write(' and '), write(B), nl,
  probe(A,VA), asserta(probe(A,VA)),
  probe(B,VB), asserta(probe(B,VB)),
  Y is VA /\ VB.

probe(X,Y) :-
  or(A,B,X), !,
  write(X), write(' = '), write(A), write(' or '), write(B), nl,
  probe(A,VA), asserta(probe(A,VA)),
  probe(B,VB), asserta(probe(B,VB)),
  Y is VA \/ VB.

probe(X,Y) :-
  lshift(A,B,X), !,
  write(X), write(' = '), write(A), write(' lshift '), write(B), nl,
  probe(A,VA), asserta(probe(A,VA)),
  Y is VA << B.

probe(X,Y) :-
  rshift(A,B,X), !,
  write(X), write(' = '), write(A), write(' rshift '), write(B), nl,
  probe(A,VA), asserta(probe(A,VA)),
  Y is VA >> B.

probe(X,Y) :-
  not(A,X), !,
  write(X), write(' = '), write(' not '), write(A), nl,
  probe(A,VA), asserta(probe(A,VA)),
  Y is \ VA /\ 65535.

/*
wire(x,123).
wire(y,456).
and(x,y,d).
or(x,y,e).
lshift(x,2,f).
rshift(y,2,g).
not(x,h).
not(y,i).
*/

