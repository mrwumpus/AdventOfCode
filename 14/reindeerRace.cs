using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace net.sinawali.adventofcode.reindeer
{
  public class Start
  {
    public static void Main (string[] args)
    {
      if (args.Length == 0 ) {
       // 
      }

      var fileName = args[0];

      var file = new StreamReader(fileName);
      string line;
      var reindeers = new List<Reindeer>();
      char[] delimiters = {' '};
      while ( (line = file.ReadLine()) != null )
      {
        var words = line.Split(delimiters);

        if (words.Length == 15)
        {
          reindeers.Add(new Reindeer()
              {
                Name = words[0],
                FlySpeed = int.Parse(words[3]),
                FlyDuration = int.Parse(words[6]),
                RestDuration = int.Parse(words[13])
              });
         }
      }
      file.Close();

      var delta = 2503; //seconds

      var max = reindeers.Select(r => r.distanceAfter(delta)).Max();

      Console.WriteLine("Max distance: {0} km", max);
      //foreach (var r in reindeers)
      //{
        //Console.WriteLine("{0} flew {1}km", r.Name, r.distanceAfter(delta));
      //}
      //
      
      var scores = new Dictionary<string,int>();
      for (var i = 1; i <= delta; ++i)
      {
        foreach (var name in Reindeer.winners(reindeers, i)) {
          if(scores.ContainsKey(name)) {
            var score = scores[name];
            scores[name] = score+1;
          } else {
            scores.Add(name, 1);
          }
        }
      }

      //foreach (KeyValuePair<string, int> score in scores)
      //{
        //Console.WriteLine("{0} scored {1}", score.Key, score.Value);
      //}
      
      var maxScore = scores.Values.Max();
      Console.WriteLine("Max score: {0}", maxScore);
    }
  }

  public class Reindeer
  {
    public string Name { get; set; }
    public int FlySpeed { get; set; }
    public int FlyDuration { get; set; }
    public int RestDuration { get; set; }

    public int distanceAfter(int secs)
    {
      var cycleDuration = FlyDuration + RestDuration;
      var cycles = secs/cycleDuration;
      var remaining = secs % cycleDuration;

      var flySecs = FlyDuration * cycles + Math.Min(FlyDuration, remaining);

      return flySecs * FlySpeed;
    }

    public static IList<string> winners(IList<Reindeer> race, int secs)
    {
      return race.Aggregate(Tuple.Create(new List<string>(),0),
          (winner, r) =>
          {
            var curDist = r.distanceAfter(secs);
            if(curDist > winner.Item2)
            {
              var names = new List<string>();
              names.Add(r.Name);
              return Tuple.Create(names, curDist);
            }

            if(curDist == winner.Item2) {
              var names = winner.Item1;
              names.Add(r.Name);
              return Tuple.Create(names, curDist);
            }

            return winner;
            }).Item1;
    }
  }
}

