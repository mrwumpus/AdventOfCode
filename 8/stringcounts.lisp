(require :quicklisp)
(ql:quickload "cl-ppcre")

(with-open-file (stream "input")
  (let ((c 0)(ec 0)(lines 0))
    (loop
      for line = (read-line stream nil 'eof)
      until (eq line 'eof)
      for escline = 
        (cl-ppcre:regex-replace-all "\\\\x[a-f0-9]{2}" 
          (cl-ppcre:regex-replace-all "\\\\(\\\\)|\\\\(\")"  
              (subseq line 1 (1- (length line))) ".") "-")
      do 
        (incf lines)
        ;(format t "~a~%" line)
        (setf c (+ c (length line)))
        ;(format t "~a~%" escline)
        (setf ec (+ ec (length escline)))
        ;(format t "c: ~a, ec: ~a diff: ~a~%" c ec (- c ec))
      finally (format t  "Decoded ~A lines: ~A~%" lines (- c ec)))))

(with-open-file (stream "input")
  (let ((c 0)(ec 0)(lines 0))
    (loop
      for line = (read-line stream nil 'eof)
      until (eq line 'eof)
      for escline = 
        (format nil "\"~a\""
          (cl-ppcre:regex-replace-all "(\")|(\\\\)" line "\\\\\\1\\2"))
      do 
        (incf lines)
        ;(format t "~a~%" line)
        (setf c (+ c (length line)))
        ;(format t "~a~%" escline)
        (setf ec (+ ec (length escline)))
        ;(format t "c: ~a, ec: ~a diff: ~a~%" c ec (- c ec))
      finally (format t  "Encoded ~A lines: ~A~%" lines (- ec c)))))
