Object subclass: Elevator [
    | floor |
    <comment:
        'This is the evevator class that can take a file of directions to determine the resulting floor'>
]

Elevator class extend [
    new [
        | e |
        <category: 'instance creation'>
        e := super new.
        e init.
        ^ e
    ]
]

Elevator extend [
    init [
        <category: 'initialization'>
        floor := 0
    ]

    floor [
        <category: 'accessing'>
        ^ floor
    ]

    changeFloor: directive [
        <category: 'movement'>
        directive = $( 
        ifTrue: [ floor := floor + 1 ]
        ifFalse: [
            directive = $) 
            ifTrue: [ floor := floor - 1 ]
            ifFalse: [ ^self error: 'Invalid directive: ', directive ].
        ].

        ^self
    ]

    changeFloors: directives [
        <category: 'movement'>
        directives do: [ :directive | self changeFloor: directive ].

        ^self
    ]

    stopAtFloor: floor using: directives [
        | cur |
        <category: 'movement'>
        1 to: directives size do: [ :idx |
            cur := directives at: idx.
            
            (self changeFloor: cur) floor = floor
                ifTrue: [ ^idx ].
        ].

        ^ -1
    ]

    withFile: filename linesDo: aBlock [
        | f |
        <category: 'execution'>
        f := FileStream open: filename mode: FileStream read.
        f linesDo: aBlock.
        f close.
    ]

]

e := Elevator new.

e withFile: 'input' linesDo: 
    [ :line | e changeFloors: line ].

Transcript show: 'At Floor: '; print: (e floor); cr.

e init.

e withFile: 'input' linesDo:
    [ :line | 
      Transcript show: 'Hit Floor: ';
               print: (e stopAtFloor: -1 using: line);
               cr ].
